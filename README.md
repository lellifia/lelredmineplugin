# README #

Redmine's plugin that generates a LEL by processing the issues.

## **Introduction** ##
Redmine is a platform used by many organizations to manage and control projects in agile development methods. One of the biggest concepts in agile development is the user stories, but this application does not integrate any feature for them by default.

<no se que quisieron decir>

In order to satisfy these needs, we develop this plugin that gives the user some functionalities related to user stories:
<ustedes hicieron un plugin para user stories?>

- User stories generation.
- User stories index.
- User stories filter.

We were able to create this plugin thanks to a natural language processing (NLP) application: **LELParser**, developed by [LIFIA Laboratory](http://www.lifia.info.unlp.edu.ar/lifia/en)


The LELParser parses user stories' content and brings us a dictionary that collects all the relevant data extracted from the given information. In other words, it collects subjects, verbs and objects, associated to the original input in order to ease the subsequent processing <associated to users stories in order easy project management tasks>. 


## **Installation** ##

This repository includes a full version of Redmine with the **UserStories** plugin. In order the get the app started, follow the next steps:

- Clone the git project:

```bash
git clone `https://bitbucket.org/lellifia/lelredmineplugin
```

- Start the [Docker](https://docs.docker.com/) service:

```bash
sudo systemctl start docker
```

- Set up database schema and default data:
*It is necessary to have the **[docker-compose](https://docs.docker.com/compose/)** tool installed.*

```bash
sudo mkdir /var/mysql-docker
sudo mkdir /var/mysql-docker/redmine
cd lelredmineplugin
./initialize.sh
docker-compose up
"ctrl-c once the compose is completed. It's time to build the database"
docker-compose run redmine rake db:create
docker-compose run redmine rake db:migrate
docker-compose run redmine rake redmine:plugins:migrate
docker-compose run redmine rake redmine:load_default_data
```
- Start docker containers with **docker-compose** tool.

```bash
docker-compose up
```

- Access the Redmine app through `http://localhost:3000`. The port 3000 is used by Ruby on Rails applications by default.


## **Background** ##

### **Redmine** ###
[Redmine](http://www.redmine.org/) is a flexible project management web application. Written using the Ruby on Rails framework, it is cross-platform and cross-database.

Redmine is open source and released under the terms of the GNU General Public License v2 (GPL).


## **Solution** ##

For the development of the plugin we rely on [this official tutorial](http://www.redmine.org/projects/redmine/wiki/Plugin_Tutorial) in which is explained in detail how to create an configure a plugin for Redmine.

Once the plugin was created, we designed the **UserStory** model and implemented it. It has three main attributes:

- roles
- actions
- objects

Each of these attributes is persisted in BBDD as serialized arrays.

As secondary, but not less important information, a UserStory has a project and an issue attached. Moreover, it is only created in case an issue is created.


The flow of operation of the plugin is the following:

- It triggers an issue creation event.
- It sends a request to the LELParser application with the **issue subject** and an **id**.
- It receives the response and parses the body, getting **roles**, **actions** and **objects**.
- It generates the UserStory object associated with the issue with the previous information and persists it.

This information has no sense unless it is accessible by the users. So we created a new controller and a view for it: the *user stories* section can be reached in three different ways:

1. Using the link in the main application menu.
2. Using the link in the **project** menu.
3. Using the link in the admin menu.

Once an user accesses the view, an index of user stories and a filter will be presented. *(In case it is used the project menu link, the filter will be using the project id by default)*

Here it is a screenshot of the plugin main view.
![user_stories.png](https://bitbucket.org/repo/jqoddj/images/452380921-user_stories.png)


As it is shown in the picture above, the index shows the user stories, with their roles, actions, objects, project and issue.

## **User guide** ##

Redmine is a flexible project management web application. It manages **project profiles**, **user permissions** and **issues tracking**.

Before you let loose your development team, you'll want to set up a **project** for them to work on. This can be done as the Admin user (username and password are both "admin"). Click on Projects (upper left), then on New Project (upper right). Fill in all the data. A description of some of the fields can be found here. The only box that might be confusing is the Project Identifier -- this is used internally by Redmine (for URLs and other things).
![new_project.png](https://bitbucket.org/repo/jqoddj/images/4031179477-new_project.png)


You'll want to get a few **users registered**, so that you can assign them to the project. Note that by default, you must activate users manually. So after a user fills out the registration page, you must log in as Admin, navigate to Administration:Users, and set the Filter to All. Activate users as necessary.
![overview.png](https://bitbucket.org/repo/jqoddj/images/2360808551-overview.png)


Once active, you can assign a user to a project. When you do this you can specify one or more roles that they play. The default options for roles are: manager, developer and reporter.

The Project overview will give you an overview of the whole project. On left upper side in the Issue tracking area you will get an overview of how many tasks are open and closed for each tracker (which is how you split your issues into different types - common ones are Bug, Feature, Defect or etc.) specified for the project. In the Members area you can see who are the members of the given project and in particular who is the administrator of the project. In the Latest news area you can see which are the latest news for the particular project.

**Issues** are the heart of the Redmine model. An issue is bound to a project, owned by a user, can be related to a version. From a selected issue page, you can see the work in progress that is done to fix or complete the issue. The messages are displayed in chronological order, (to change the order - see the setting in 'My Accounts'). It is possible to quote others' messages as well as to edit yours. People can create a new issue when they meet the roles and permissions configured by the Redmine Administrator.
When creating a new issue, one of the most important items is the tracker field, which defines the nature of the issue. By default, Redmine comes with three different trackers: bug, feature, and support.
![issues.png](https://bitbucket.org/repo/jqoddj/images/1360553864-issues.png)


**User stories** are one of the primary development artifacts for Scrum and Extreme Programming (XP) project teams. A user story is a very high-level definition of a requirement, containing just enough information so that the developers can produce a reasonable estimate of the effort to implement it. User stories are written by or for business users or customers as a primary way to influence the functionality of the system being developed.

The plugin presented in this guide associates user stories with issues: whenever an user creates an issue, a user story is automatically generated and associated to it. Each user story contains information related with its role/s, verb/s and object/s. Besides, users can filter them using roles, verbs and/or objects, for a project or all of them.

Issues may be created with a specific subject format, in order to process its content. Issues' subject should have the following format:

> As a **role**, I want **goal/desire** so that **benefit**

For example, this could be a subject for a valid issue:

> As a moderator I want to create a new game by entering a name and an optional description So that I can start inviting estimators



## **Conclusions** ##

This plugin is fully functional and can be included into any Redmine server because it has not any dependences with another plugin. The user story generation is transparent to the user, so they only need to know how to create issues and take care about the issue's subject structure.

Moreover, the plugin can be included into an installation which has projects and issues already loaded. It provides a *rake task*, which is a Make-like program implemented in Ruby, that generates user stories for all the issues.

The plugin can potentially be improved with advanced features. A list of possible improvements is attached:

- Generation of a format verifier that would let the user know if an issue's subject has a correct-formatted text that could be interpreted by the LELParser.
- Integration with any existing user stories plugin which might be known by the Redmine community.

## **Annex** ##

In case you already have running a Redmine application and you only want to add the User Stories plugin. You can follow the next steps:

1. Copy the ``plugins/user_stories`` folder into your project ``plugins`` folder.
2. Run ``rake redmine:plugins:migrate``
3. Run ``rake user_stories:load_all`` (this command will associate user stories to all your issues)

After these three steps, the plugin should be working fine.