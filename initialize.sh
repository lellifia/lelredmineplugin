#!/bin/bash

echo -e "\e[Updating repository and submodules..."

rm redmine/config/database.yml
rm redmine/Dockerfile
rm lel_agile/LELParser/Dockerfile

git pull
git submodule init
git submodule update

cd redmine
git checkout master
git submodule init
git submodule update
cd ../lel_agile
git checkout master
git reset --hard 9d3594c6cfaf1f1cd741d65c01b0e33d997252f7
cd ..

cp database.yml	redmine/config/
cp dockerfile-redmine redmine/Dockerfile
cp dockerfile-lel lel_agile/LELParser/Dockerfile

echo -e "\e[32mFinished!"
echo -e "\e[39m"
